import sys
import statistics
import time

import requests


def data_analysis(key, value):
    """ Query targetvalidation API in order to retrieve required information, print stdout Max, Min, Ave and
    Standard Deviation, based on value of 'association_score.overall' in response data.
    Request is made in batches of 100 items per batch.
    Note: Goes on sleep for 10 secs if request quote exceed limit.
    :param key: query param e.g, target or disease
    :param value: value for query params e.g, 'ENSG00000157764'
    :return: result dict with min, max, ave and stddev
    """
    request_url = 'https://www.targetvalidation.org/api/latest/public/association/filter'
    params = {
        'from': 0,
        'size': 10000,  # Max items can be requested
        key: value
    }

    overall_score = []

    while True:
        # Continue while all the records are not retrieved
        api_request = requests.get(request_url, params=params)

        try:
            api_request.raise_for_status()
        except Exception as exc:
            if api_request.status_code == 429:
                print("Request quota is exceeded! going on sleep for 5 secs ...")
                time.sleep(5)
                continue
            sys.stderr.write("API Exception: %s" % exc)

        try:
            response_data = api_request.json()
        except (AttributeError, KeyError) as exc:
            sys.stderr.write(exc)

        overall_score.extend([data['association_score']['overall'] for data in response_data['data']])

        if response_data['total'] > params['from'] + params['size']:
            params['from'] = params['from'] + params['size']
        else:
            break

    if overall_score:
        return {
            'max': max(overall_score),
            'min': min(overall_score),
            'ave': statistics.mean(overall_score),
            'stddev': statistics.pstdev(overall_score)
        }

    return None
