import sys
import unittest
from pprint import pprint
from optparse import OptionParser

from opentargets import data_analysis
from tests.test_opentargets import TestOpenTargets


def prnt_results(data):
    if data:
        sys.stdout.write("Maximum: %s\n" % data['max'])
        sys.stdout.write("Minimum: %s\n" % data['min'])
        sys.stdout.write("Average: %s\n" % data['ave'])
        sys.stdout.write("Standard Deviation: %s\n" % data['stddev'])
    else:
        sys.stdout.write("Zero results found for the query!")


def main():
    parser = OptionParser()
    parser.add_option("-t", "--target", dest="target",
                      help="Will run an analysis for a target.")
    parser.add_option("-d", "--disease", dest="disease",
                      help="Will run an analysis for a disease.")

    parser.add_option("--test", dest="test", action='store_true',
                      help="Will run a suite of tests.")

    options, args = parser.parse_args()

    if options.target:
        sys.stdout.write('Analysis for target value "%s"\n' % options.target)
        prnt_results(data_analysis('target', options.target))
    elif options.disease:
        sys.stdout.write('Analysis for disease value "%s"\n' % options.disease)
        prnt_results(data_analysis('disease', options.disease))
    elif options.test:
        runner = unittest.TextTestRunner(verbosity=2)
        result = runner.run(unittest.makeSuite(TestOpenTargets))
        print('Tests run ', result.testsRun)
        print('Errors ', result.errors)
        pprint(result.failures)

    else:
        sys.stderr.write("Please choice the options. For more info see choice option--help.\n")


if __name__ == "__main__":
    main()
