# Open Target API access

The goal of this application is to fetch API data from targetvalidation.org (docs available at: https://www.targetvalidation.org/documentation/api).


## Instructions
1. Clone the project and go to project dir,
```git clone git@bitbucket.org:aedil12155/opentargets.git opentargets && cd opentargets```
2. Install application, ```python setup.py.install``` (it is recommended to use virtualenv for isolated env of project) 


## Executing Queries / How to use
There are three 3 options available -t for target query, -d for disease query and --test to execute test suite.

To see available options,
```my_code_test --help```

### Examples:
* Querying target (command line arg is -t),
```my_code_test -t ENSG00000157764```

* Query disease (command line arg is -d),
```my_code_test -d EFO_0002422```

* Run Test suite,
```my_code_test --test```