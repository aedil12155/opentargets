import unittest

from opentargets import data_analysis


class TestOpenTargets(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_target_results(self):
        query = 'ENSG00000157764'
        result = data_analysis('target', query)
        self.assertEqual(result['max'], 1, 'Max value unexpected!')
        self.assertEqual(result['min'], 0.004, 'Min value unexpected!')
        self.assertEqual(round(result['ave'], 15), round(0.3465149105570467, 15), 'Average unexpected!')
        self.assertEqual(round(result['stddev'], 15), round(0.39072594922932469, 15), 'Average unexpected!')

    def test_disease_results(self):
        query_data = {
            'EFO_0002422': {
                'max': 1.0,
                'min': 0.004,
                'ave': 0.057526245349015792,
                'stddev': 0.11123883562986155,
            },
            'EFO_0000616': {
                'max': 1.0,
                'min': 0.004,
                'ave': 0.40494078911465325,
                'stddev': 0.33650774582807524,
            },
        }

        for query, data in query_data.items():
            result = data_analysis('disease', query)
            self.assertEqual(result['max'], data['max'], 'Max value unexpected!')
            self.assertEqual(result['min'], data['min'], 'Min value unexpected!')
            self.assertEqual(round(result['ave'], 15), round(data['ave'], 15), 'Average unexpected!')
            self.assertEqual(round(result['stddev'], 15), round(data['stddev'], 15), 'Average unexpected!')

    def test_target_results_with_inavlid_query(self):
        query = 'somefakequery'

        result = data_analysis('disease', query)
        self.assertIsNone(result, 'Unexpected response, must be None!')


if __name__ == '__main__':
    unittest.main()
