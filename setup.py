#!/usr/bin/env python

import os
from pip.req import parse_requirements

from setuptools import setup, find_packages


cur_dir = os.path.dirname(os.path.realpath(__file__))

# install requirements
requirements_path = os.path.join(cur_dir, 'requirements.txt')
install_requirements = parse_requirements(requirements_path, session=False)
required_packages = [str(ir.req) for ir in install_requirements]


if __name__ == '__main__':
   setup(
       name='my_code_test',
       version='0.0.1',
       description='''''',
       long_description='''''',
       author="Adeel Younas",
       author_email="aedil12155@gmail.com",
       license='',
       classifiers=[
           'Development Status :: 3 - Alpha',
           'Programming Language :: Python'
       ],
       scripts=['my_code_test.py'],
       packages=find_packages(),
       entry_points={
          'console_scripts': [
              'my_code_test = my_code_test:main',
          ],
       },
       #include_package_data=True,
       #package_data={
       #    'izin': ['izin/templates', 'izin/static']
       #},
       install_requires=required_packages,
       zip_safe=False
   )